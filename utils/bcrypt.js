const bcrypt = require('bcryptjs');

function encrypt(password) {
    return new Promise(passhash => {
        bcrypt.hash(password, 10, (err, hash) => {
            if (err) {
                Promise.reject("ผิดพลาด")
            }
            passhash(hash)
        })
    }, 1000)
}

function decrypt(dbpassword, password) {
    return new Promise(passhash => {
        bcrypt.compare(password, dbpassword, (err, hash) => {
            if (err) {
                Promise.reject("รหัสผ่านผิด")
            }
            passhash(hash)
        })
    }, 1000)
}

module.exports = {
    encrypt,
    decrypt
}