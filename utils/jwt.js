const jwt = require('jsonwebtoken')
require('dotenv').config()

function jwtgenerate(uuid, username) {
    return new Promise(jwtgen => {
        if (uuid && username) {
            const token = jwt.sign({
                uuid: uuid,
                username: username
            }, process.env.JWT_KEY, { expiresIn: "7d" })
            jwtgen(token)
        }
    }, 1000)
}

function jwtverify(token) {
    return new Promise(jwtver => {
        if (token) {
            const decodejwt = jwt.verify(token, process.env.JWT_KEY)
            jwtver(decodejwt)
        }
    }, 1000)
}

function jwtdecode(token) {
    return new Promise(jwtdecode => {
        if (token) {
            var decoded = jwt.decode(token, { complete: true })
            jwtdecode(decoded)
        }
    }, 1000)
}

module.exports = {
    jwtgenerate,
    jwtverify,
    jwtdecode
}