const { generate } = require('referral-codes')

function CodeGen(){
    return new Promise(serialGen => {
        const code = generate({
            length: 15,
            count: 1,
            charset: '0123456789',
        })
        serialGen(code[0])
    }, 1000)
}

function SerialCodeGen(){
    return new Promise(serialGen => {
        const code = generate({
            length: 15,
            count: 1,
            charset: '0123456789',
        })
        serialGen(code[0] + 'TH')
    }, 1000)
}

module.exports = {
    CodeGen,
    SerialCodeGen
}