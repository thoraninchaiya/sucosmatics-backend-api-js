const db = require('../../db/index')
const { decrypt, encrypt } = require('../../utils/bcrypt')
const { jwtgenerate } = require('../../utils/jwt')
const { Users } = db
const { v4: uuidv4 } = require('uuid')

const Login = async function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return res.status(400).send({
            status: 400,
            message: "ชื่อผู้ใช้งานหรือรหัสผ่านผิด!"
        })
    }
    const result = await Users.findOne({
        where: { user_email: req.body.email }
    })
    if (result) {
        const checkpassword = await decrypt(result.user_password, req.body.password)
        if (checkpassword) {
            const token = await jwtgenerate(result.user_uuid, result.user_email)
            return res.send({
                token: token,
                user: {
                    user_email: result.user_email,
                    user_uuid: result.user_uuid,
                    user_fname: result.user_fname,
                    user_lname: result.user_lname,
                }
            })
        } else {
            return res.status(400).send({
                status: 400,
                message: "รหัสผ่านผิด!"
            })
        }

    }
    else {
        return res.status(400).send({
            status: 400,
            message: "ไม่พบผู้ใช้!"
        })
    }
}

let Register = async function (req, res, next) {
    if (!req.body.email || !req.body.password || !req.body.fname || !req.body.lname || !req.body.phone || !req.body.address) {
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูลให้ครบ!"
        })
    }
    const searchresult = await Users.findOne({
        where: { user_email: req.body.email }
    })
    if(!searchresult){
        const uuid = await uuidv4()
        const encryptpassword = await encrypt(req.body.password)
        const result = await Users.create({
            user_email: req.body.email,
            user_uuid: uuid,
            user_password: encryptpassword,
            user_fname: req.body.fname,
            user_lname: req.body.lname,
            user_phone: req.body.phone,
            user_address: req.body.address,
            // user_status: 1,
            // user_role: 1
        })
        if (result){
            return res.send({
                message: "สมัครสมาชิกสำเร็จ!"
            })
        }else{
            return res.status(500).send({
                status: 500,
                message: "ผิดพลาด"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "มีผู้ใช้งานแล้ว!"
        })
    }


}

module.exports = {
    Login,
    Register
}
