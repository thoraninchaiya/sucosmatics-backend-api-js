const db = require('../../db/index')
const { Order, Order_detail, Product, Users, Cart, Cart_detail } = db
const { Op } = require('sequelize')
const { CodeGen, SerialCodeGen } = require('../../utils/serialGen')
const { Delivery, Delivery_company, Payment, Receipt, Receipt_detail } = require('../../db/index')
const { endPoint, imgProduct } = require('../../config')

const GetOrder = async function (req, res, next) {
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "ไม่พบออเดอร์"
        })
    }
    const user = await Users.findOne({
        where: { user_uuid: req.userData.uuid }
    })
    if(user){
        const orders = await Order.findOne({
            where: { [Op.and]:[{order_id: req.params.id}, { user_id: user['user_id']}] },
            include: {
                model: Delivery,
                include: Delivery_company
            }
        })
        if(orders){
            const getDetail = await Order_detail.findAll({
                where: { order_id: orders['order_id'] },
                include: Product
            })
            const orderDetails = []
            for (let index = 0; index < getDetail.length; index++) {
                const orderDetail = {
                    order_qty: getDetail[index]['order_qty'],
                    product_name: getDetail[index]['product']['product_name'],
                    product_unit_price: getDetail[index]['product_unit_price'],
                    product_total_amt: getDetail[index]['product']['product_price'] * getDetail[index]['order_qty'],
                    product_img: endPoint + imgProduct + getDetail[index]['product']['product_image']
                }
                orderDetails.push(orderDetail)
            }
            const data = {
                order_id: orders['order_id'],
                order_serial: orders['order_serial'],
                order_date: orders['order_date'],
                order_total_amt: orders['order_total_amt'],
                order_status: orders['order_status'],
                delivery_id: orders['delivery']['delivery_company']['delivery_company_name'],
                product: orderDetails
            }
            return res.json(data)
        }else{
            return res.status(400).send({
                status: 400,
                message: "ไม่พบออเดอร์"
            }) 
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบผู้ใช้งาน"
        })
    }
}

const GetOrders = async function (req, res, next) {
    const user = await Users.findOne({
        where: { user_uuid: req.userData.uuid }
    })
    if(user){
        const getOrders = await Order.findAll({
            where: { user_id: user['user_id']  }
        })
        if(getOrders.length > 0){
            return res.json(getOrders)
        }else{
            return res.status(400).send({
                status: 400,
                message: "ไม่พบออเดอร์"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบผู้ใช้งาน"
        })
    }
}

const CreateOrder = async function (req, res, next) {
    if(!req.body.delivery){
        return res.status(400).send({
            status: 400,
            message: "กรุณาเลือกบริษัทขสนส่ง"
        })
    }
    const user = await Users.findOne({
        where: { user_uuid: req.userData.uuid }
    })
    if(user){
        const findcart = await Cart.findOne({
            where: { [Op.and]:[{ cart_status: 1},{user_id: user.user_id}]}
        })
        if(findcart){
            const Code = await CodeGen()
            const createDelivery = await Delivery.create({
                delivery_company_id: req.body.delivery
            })
            if(createDelivery){
                const createOrder = await Order.create({
                    order_serial: Code,
                    order_total_amt: findcart['cart_amt'],
                    delivery_id: createDelivery['delivery_id'],
                    user_id: user['user_id']
                })
                if(createOrder){
                    const fineCartDetail = await Cart_detail.findAll({
                        where: { cart_id: findcart['cart_id'] },
                        include: Product
                    })
                    if(fineCartDetail.length > 0){
                        const cartDetail = []
                        for (let index = 0; index < fineCartDetail.length; index++) {
                            const cartDataDetail = {
                                order_qty: fineCartDetail[index]['cart_detail_qty'],
                                product_unit_price: fineCartDetail[index]['product']['product_price'],
                                product_total_amt: findcart['cart_amt'],
                                product_id: fineCartDetail[index]['product']['product_id'],
                                order_id: createOrder['order_id'],
                            }
                            cartDetail.push(cartDataDetail)
                        }
                        const addOrderDetail = await Order_detail.bulkCreate(cartDetail)
                        if(addOrderDetail){
                            return res.send({
                                order_id: createOrder['order_id'],
                                order_serial: createOrder['order_serial'],
                                message: "สร้างออเดอร์สำเร็จ รอการชำระเงิน"
                            })
                        }
                    }
                }
            }
        }   
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบผู้ใช้งาน"
        })
    }
}

const OrderPayment = async function (req, res, next) {
    console.log(req);
    console.log(req.image);
    console.log(req.body);
    if(!req.body.orderid, !req.body.total, !req.body.time, !req.files){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูลให้ครบ"
        })
    }
    const findOrder = await Order.findOne({
        where: { [Op.and]: [{order_id: req.body.orderid}, {order_status: 1}] }
    })
    if(findOrder){
        var file = req.files.image
        var fileanmemd5 = file.md5
        var type = file.mimetype
        var cuttype = type.split('/')
        file.mv('./public/img/payment/'+ fileanmemd5 + "." + cuttype[1], async function(err){
            const createPayment = await Payment.create({
                payment_total: req.body.total,
                payment_image: fileanmemd5 + "." + cuttype[1],
                payment_time: req.body.time,
                order_id: findOrder['order_id'],
            })
            if(createPayment){
                const updateOrderPayment = await Order.update({
                    order_status: 2
                }, {where: { order_id: findOrder['order_id'] }})
                return res.send({
                    message: "อัพโหลดสลิปสำเร็จ"
                }) 
            }
        })
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบออเดอร์"
        }) 
    }
}

const OrderCancel = async function (req, res, next) {
    if(!req.body.orderid){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูลให้ครบ"
        })
    }
    const findOrder = await Order.findOne({
        where: { [Op.and]: [{order_id: req.body.orderid}, {order_status: 1}] }
    })
    if(findOrder){
        const updateOrderPayment = await Order.update({
            order_status: 4
        }, {where: { order_id: findOrder['order_id'] }})
        if(updateOrderPayment){
            return res.send({
                message: "ยกเลิกออเดอร์สำเร็จ"
            }) 
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบออเดอร์"
        }) 
    }
}

const AdminOrderAll = async function (req, res, next) {
    const updateOrderPayment = await Order.findAll({})
    if(updateOrderPayment.length > 0){
        return res.json(updateOrderPayment)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบออเดอร์"
        }) 
    }
}

const AdminOrderSuccess = async function (req, res, next) {
    if(!req.body.orderid){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูลให้ครบ"
        })
    }
    const findOrder = await Order.findOne({
        where: { [Op.and]: [{order_id: req.body.orderid}, {order_status: 2}] },
        include: [{
            model: Order_detail,
            include: Product
        }]
    })
    if(findOrder){
        // const updateOrderDelivery = await Order.update({
        //     order_status: 5
        // }, {where: { order_id: findOrder['order_id'] }})
        // if(updateOrderDelivery){
            const findUser = await Users.findOne({
                where: { user_id: findOrder['user_id']}
            })
            if(findUser){
                const serial = await SerialCodeGen()
                const createReceipt = await Receipt.create({
                    receipt_serial: serial,
                    receipt_total_amt: findOrder['order_total_amt'],
                    user_address: findUser['user_address'],
                    user_name: findUser['user_fname'] + ' ' + findUser['user_lname'],
                })
                if(createReceipt){
                    const orderDetail = findOrder['order_details']
                    console.log(orderDetail);
                    // const receiptDetails = []
                    // for (let index = 0; index < orderDetail.length; index++) {
                    //     // console.log(orderDetail[index]['product']['product_name']);
                    //     const receiptDetail = {
                    //         receipt_qty: findOrder['order_qty'],
                    //         product_name: orderDetail[index]['product']['product_name'],
                    //         product_unit_price: orderDetail[index]['product']['product_price'],
                    //         product_total_amt: findOrder['product_total_amt'],
                    //         product_serial: orderDetail[index]['product']['product_serial'],
                    //         receipt_id: createReceipt['receipt_id'],
                    //     }
                    //     receiptDetails.push(receiptDetail)
                    // }
                    // const addReceiptDetail = await Receipt_detail.bulkCreate(receiptDetails)
                    // console.log(addReceiptDetail);
                }
            }
        // }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบออเดอร์"
        }) 
    }
}

module.exports = {
    GetOrder,
    GetOrders,
    CreateOrder,
    OrderPayment,
    OrderCancel,
    AdminOrderAll,
    AdminOrderSuccess
}