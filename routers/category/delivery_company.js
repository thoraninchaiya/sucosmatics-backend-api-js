const db = require('../../db/index')
const { Delivery_company } = db
const { Op } = require('sequelize')



const GetAllComDelivery = async function (req, res, next) {
    const getresult = await Delivery_company.findAll({
      where: { [Op.or]:[ {delivery_company_status: 1}, {delivery_company_status: 2} ] }
    })
    if(getresult && getresult.length > 0){
        return res.json(getresult)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const AddComDelivery = async function (req, res, next) {
    if(!req.body.name || !req.body.status){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูลให้ครบ"
        })
    }
    const find = await Delivery_company.findOne({
        where: { delivery_company_name: req.body.name }
    })
    if(find){
        return res.status(400).send({
            status: 400,
            message: "มีข้อมูลอยู่แล้ว"
        })
    }
    const insert = await Delivery_company.create({
        delivery_company_name: req.body.name,
        delivery_company_status: req.body.status
    })
    if(insert){
        return res.send({
            message: "เพิ่มข้อมูลสำเร็จ"
        })
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่สามารถเพิ่มข้อมูลได้"
        })
    }
}

const UpdateComDelivery = async function (req, res, next) {
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูลให้ครบ"
        })
    }
    const find = await Delivery_company.findOne({
        where: { delivery_company_id: req.params.id }
    })
    if(find){
        const update = await Delivery_company.update({ delivery_company_name: req.body.name, delivery_company_status: req.body.status }, {where: { delivery_company_id: req.params.id} })
        if(update){
            return res.send({
                message: "แก้ไขข้อมูลสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "ไม่สามารถแก้ไขได้"
            })  
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const RemoveComDelivery = async function (req, res, next) {
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูลให้ครบ"
        })
    }
    const find = await Delivery_company.findOne({
        where: { delivery_company_id: req.params.id }
    })
    if(find){
        const update = await Delivery_company.destroy({where: { delivery_company_id: req.params.id}})
        if(update){
            return res.send({
                message: "ลบข้อมูลสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "ไม่สามารถลบได้"
            })  
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetAllActiveDelivery = async function (req, res, next) {
    const getresult = await Delivery_company.findAll({
        where: {delivery_company_status: 1}
      })
      if(getresult && getresult.length > 0){
          return res.json(getresult)
      }else{
          return res.status(400).send({
              status: 400,
              message: "ไม่มีข้อมูล"
          })
      }
}

module.exports = {
    GetAllComDelivery,
    UpdateComDelivery,
    RemoveComDelivery,
    GetAllActiveDelivery,
    AddComDelivery
}