const db = require('../../db/index')
const { Category } = db
const { Op } = require('sequelize')
const { Product } = require('../../db/index')

const GetActviveCategorys = async function (req, res, next) {
    const getcategorys = await Category.findAll({
        where: { category_status: 1}
    })
    if(getcategorys.length > 0){
        const categorys = []
        for (let index = 0; index < getcategorys.length; index++) {
            const category = {
                category_id: getcategorys[index]['category_id'],
                category_name_th: getcategorys[index]['category_name_th'],
                category_name_en: getcategorys[index]['category_name_en'],
            }
            categorys.push(category)
        }
        return res.json(categorys)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetAllCategory = async function (req, res, next) {
    const getcategorys = await Category.findAll()
    if(getcategorys){
        return res.json(getcategorys)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetCategory = async function (req, res, next) {
    if(!req.params.id){
        return res.status(400).send({
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const findcategory = await Category.findOne({
        where: { category_id: req.params.id }
    })
    if (findcategory){
        return res.json(findcategory)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const AddCategory = async function (req, res, next) {
    if( !req.body.nameth){
        return res.status(400).send({
            status: 400,
            message: "ข้อมูลไม่ครบถ้วนกรุณากรอกใหม่!"
        })
    }
    const findcategory = await Category.findOne({
        where: { category_name_th: req.body.nameth}
    })
    if (findcategory){
        return res.status(400).send({
            staus: 400,
            message: "ข้อมูลมีอยู่แล้ว"
        })
    }
    const result = await Category.create({
        category_name_th: req.body.nameth,
        category_status: req.body.status,
        category_name_en: req.body.nameen
    })
    if(result){
        return res.send({
            message: "เพิ่มข้อมูลสำเร็จ"
        })
    }else{
        return res.status(400).send({
            stauts: 400,
            message: "ผิดพลาดไม่สามารถเพิ่มข้อมูลได้"
        })
    }
}

const UpdateCategory = async function (req, res, next) {
    if(!req.params.id){
        return res.stauts(400).send({
            status: 400,
            message: "ไม่พบข้อมูล"
        })
    }
    const findcategory = await Category.findOne({
        where: { category_id: req.params.id}
    })
    if(findcategory){
        const update = await Category.update({ category_name_th: req.body.nameth, category_name_en: req.body.nameen, category_status: req.body.status}, { where: {category_id: req.params.id}})
        if(update){
            return res.send({
                message: "แก้ไขข้อมูลสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "แก้ไขข้อมูลไม่สำเร็จ"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบข้อมูล"
        })
    }
}

const RemoveCategory = async function (req, res, next) {
    if(!req.params.id){
        return res.status(400).send({
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const findcategory = await Category.findOne({
        where: { category_id: req.params.id}
    })
    if (findcategory){
        const usecheck = await Product.findOne({
            where: { category_id: req.params.id }
        })
        if(!usecheck){
            const removecategory = await Category.destroy({
                where: { category_id: req.params.id}
            })
            if(removecategory){
                return res.send({
                    message: "ลบข้อมูลสำเร็จ"
                })
            }else{
                return res.status(400).send({
                    status: 400,
                    message: "ลบข้อมูลผิดพลาด"
                })
            }
        }else{
            return res.status(400).send({
                status: 400,
                message: "ไม่สามารถลบได้เนื่องจากมีสินค้าใช้อยู่"
            }) 
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

module.exports = {
    GetActviveCategorys,
    GetAllCategory,
    AddCategory,
    RemoveCategory,
    UpdateCategory,
    GetCategory,
}