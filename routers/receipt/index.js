const db = require('../../db/index')
const { Order, Order_detail, Product, Users, Cart, Cart_detail, Delivery } = db
const { Op } = require('sequelize')
const { Receipt, Receipt_detail } = require('../../db/index')
const { SerialCodeGen } = require('../../utils/serialGen')

const CreateReceipt = async function (req, res, next) {
    if(!req.body.id){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกออเดอร์"
        })
    }
}

const getReceipt = async function (req, res, next) {
    
}

module.exports = {
    CreateReceipt,
    getReceipt,
}