const express = require('express')
const { isLoggedIn } = require('../middleware/auth')
const router = express.Router()

const { Login, Register } = require('./auth/index')
const { GetCarousel, RemoveCarousel, GetActviveCarousel, GetAllCarousel, AddCarousel } = require('./carousel')
const { GetItemCart, AddItemCart, RemoveItemCart } = require('./cart')
const { GetAllComDelivery, AddComDelivery, GetAllActiveDelivery, UpdateComDelivery, RemoveComDelivery } = require('./category/delivery_company')
const { GetActviveCategorys, GetAllCategory, GetCategory, AddCategory, UpdateCategory, RemoveCategory } = require('./category/product')
const { GetOrder, GetOrders, CreateOrder, OrderPayment, OrderCancel, AdminOrderAll, AdminOrderSuccess } = require('./order')
const { GetActviveProduct, GetAllProduct, GetProduct, AddProduct, UpdateProduct, RemoveProduct, GetProductByCategory, GetNewProduct, GetBestSellerProduct } = require('./product')
const { CreateReceipt } = require('./receipt')
const { GetAllProfiles, UpdateProfileById, UpdateProfileByToken, GetProfileById, GetProfileByToken, RemoveProfile } = require('./user')

router.post('/auth/login', Login) //เข้าระบบ
router.post('/auth/register', Register) //สมัครสมาชิก

//หมวดหมู่สินค้า
router.get('/category', GetActviveCategorys) //เรียกหมวดหมู่ที่เปิดใช้งาน
router.get('/categorys', GetAllCategory) //เรียกค่าทั้งหมดของหมวดหมู่สินค้า
router.get('/category/:id', GetCategory) //เรียกค่าเฉพาะ id นั้นๆ
router.post('/category', AddCategory) //เพิ่มหมวดหมู่สินค้า
router.put('/category/:id', UpdateCategory) //อัพเดตค่าหมวดหมู่สินค้า
router.delete('/category/:id', RemoveCategory) //ลบหมวดหมู่สินค้า

//เรียกหมวดหมู่บริษัทส่งสินค้า
router.get('/delivery/companys', GetAllComDelivery) // เรียกบริษัทขนส่งทั้งหมด
router.post('/delivery/company', AddComDelivery) // เพิ่มบริษัทขนส่ง
router.get('/delivery/company', GetAllActiveDelivery) // เรียกบริษัทขนส่งที่เปิดใช้งาน
router.put('/delivery/company/:id', UpdateComDelivery) // แก้ไขบริษัทขนส่ง
router.delete('/delivery/company/:id', RemoveComDelivery) // ลบบริษัทขนส่ง

//สินค้า
router.get('/product/category/:id', GetProductByCategory) //เรียกสินค้าโดยค้นหาจากหมวดหมู่
router.get('/product', GetActviveProduct) //เรียกสินค้าที่เปิดใช้งาน
router.get('/product/bestseller', GetBestSellerProduct) //เรียกสินค้าที่ขายดีและเปิดใช้งาน
router.get('/product/new', GetNewProduct) //เรียกสินค้าใหม่ที่เปิดใช้งาน
router.get('/products', GetAllProduct) //เรียกค่าทั้งหมดของสินค้าสินค้า
router.get('/product/:id', GetProduct) //เรียกค่าเฉพาะ id นั้นๆ
router.post('/product', AddProduct) //เพิ่มสินค้าสินค้า
router.put('/product/:id', UpdateProduct) //อัพเดตค่าสินค้าสินค้า
router.delete('/product/:id', RemoveProduct) //ลบสินค้าสินค้า

//โปรไฟล์
router.get('/admin/profiles', GetAllProfiles) //เรียกค่าผู้ใช้งานทั้งหมด
router.get('/profile', isLoggedIn, GetProfileByToken) //เรียกค่าผู้ใช้โดยใช้ Token
router.get('/admin/profile/:id', GetProfileById) //เรียกค่าผู้ใช้โดย id
router.put('/profile', isLoggedIn, UpdateProfileByToken) //แก้ไขข้อมูลด้วย token
router.put('/admin/profile/:id', UpdateProfileById) //แก้ไขข้อมูลด้วย id
router.delete('/profile/:id', RemoveProfile) //ลบข้อมูลด้วย id

//ตะกร้าสินค้า
router.get('/cart', isLoggedIn, GetItemCart) //เรียกค่าตะกร้าสินค้า
router.put('/cart', isLoggedIn, AddItemCart) //เพิ่มสินค้าในตะกร้า
router.delete('/cart', isLoggedIn, RemoveItemCart) //ลบสินค้าในตะกร้า

//แบนเนอร์
router.get('/carousel', GetActviveCarousel) //
router.get('/carousels', GetAllCarousel) //
router.post('/carousel', AddCarousel) //
router.delete('/carousel/:id', RemoveCarousel) //

//order
router.get('/order/:id', isLoggedIn, GetOrder) //เรียกออเดอร์โดย id
router.get('/orders', isLoggedIn, GetOrders) //เรียกออเดอร์ทั้งหมด
router.post('/order/create', isLoggedIn, CreateOrder) //สร้างออเดอร์
router.post('/order/payment', OrderPayment)
router.post('/order/cancel', OrderCancel)
router.post('/admin/order/success', AdminOrderSuccess)
router.get('/admin/orders', AdminOrderAll)

//receipt
router.post('/receipt', isLoggedIn, CreateReceipt)// สร้างใบเสร็จ


router.post('/test/upload', (req, res) => {
    console.log(req.files);
    console.log(req.body)
    if (req.files) {
        var file = req.files.test
        if (!file) {
            return res.status(400).send({
                message: "ไม่พบรูป"
            })
        }
        var fileanmemd5 = file.md5
        var type = file.mimetype
        var cuttype = type.split('/')

        file.mv('./public/img/product/' + fileanmemd5 + "." + cuttype[1], function(err) {
            if (err) {
                res.send(err)
            } else {
                res.send("file upload")
            }
        })
    }
})

//ตะกร้าสินค้า
//การสั่งซื้อ
//รีวิว



module.exports = router