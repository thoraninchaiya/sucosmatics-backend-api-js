const db = require('../../db/index')
const { Users } = db
const { Op } = require('sequelize')

const GetProfileById = async function (req, res, next){
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const getprofiles = await Users.findOne({
        where: { user_id: req.params.id }
    })
    if(getprofiles){
        const user = {
            id: getprofiles['user_id'],
            uuid: getprofiles['user_uuid'],
            email: getprofiles['user_email'],
            fname: getprofiles['user_fname'],
            lname: getprofiles['user_lname'],
            phone: getprofiles['user_phone'],
            address: getprofiles['user_address'],
            status: getprofiles['user_status'],
            create_at: getprofiles['user_create_at'],
            role: getprofiles['user_role'],
        }
        return res.json(user)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetAllProfiles = async function (req, res, next){
    const getprofiles = await Users.findAll({})
    if(getprofiles && getprofiles.length > 0){
        const usersdata = []
        for (let index = 0; index < getprofiles.length; index++) {
            const user = {
                id: getprofiles[index]['user_id'],
                uuid: getprofiles[index]['user_uuid'],
                email: getprofiles[index]['user_email'],
                fname: getprofiles[index]['user_fname'],
                lname: getprofiles[index]['user_lname'],
                phone: getprofiles[index]['user_phone'],
                address: getprofiles[index]['user_address'],
                status: getprofiles[index]['user_status'],
                create_at: getprofiles[index]['user_create_at'],
                role: getprofiles[index]['user_role'],
            }
            usersdata.push(user)
        }
        return res.json(usersdata)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetProfileByToken = async function (req, res, next){
    const getprofiles = await Users.findOne({
        where: { user_uuid: req.userData.uuid }
    })
    if(getprofiles){
        const user = {
            id: getprofiles['user_id'],
            uuid: getprofiles['user_uuid'],
            email: getprofiles['user_email'],
            fname: getprofiles['user_fname'],
            lname: getprofiles['user_lname'],
            phone: getprofiles['user_phone'],
            address: getprofiles['user_address'],
            status: getprofiles['user_status'],
            create_at: getprofiles['user_create_at'],
            role: getprofiles['user_role'],
        }
        return res.json(user)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const UpdateProfileByToken = async function (req, res, next){
    const getprofile = await Users.findOne({
        where: { user_uuid: req.userData.uuid }
    })
    if(getprofile){
        const update = await Users.update({
            user_email: req.body.email,
            user_fname: req.body.fname,
            user_lname: req.body.lname,
            user_phone: req.body.phone,
            user_address: req.body.address,
        },{ where: { user_uuid: req.userData.uuid} })
        if(update){
            return res.send({
                message: "แก้ไขสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "แก้ไขไม่สำเร็จ"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}
const UpdateProfileById = async function (req, res, next){
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const getprofiles = await Users.findOne({
        where: { user_id: req.params.id }
    })
    if(getprofiles){
        const update = await users.update({
            user_email: req.body.email,
            user_fname: req.body.fname,
            user_lname: req.body.lname,
            user_phone: req.body.phone,
            user_address: req.body.address,
            user_status: req.body.status,
            user_role: req.body.role,
        },{ where: { user_id: req.params.id} })
        if(update){
            return res.send({
                message: "แก้ไขสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "แก้ไขไม่สำเร็จ"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const RemoveProfile = async function (req, res, next){
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "ผิดพลาด"
        })
    }
    const find = await Users.findOne({
        where: { user_id: req.params.id }
    })
    if(find){
        const update = await Users.update({
            user_status: 2,
        },{ where: { user_id: req.params.id} })
        if(update){
            return res.send({
                message: "ลบสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "ลบสำเร็จไม่สำเร็จ"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบข้อมูล"
        })
    }
}

module.exports = {
    GetProfileById,
    GetAllProfiles,
    GetProfileByToken,
    UpdateProfileByToken,
    RemoveProfile,
    UpdateProfileById
}