const db = require('../../db/index')
const { Product, Category } = db
const { Op } = require('sequelize')
const { endPoint, imgProduct } = require('../../config')

const GetBestSellerProduct = async function (req, res ,next) {
    const getproducts = await Product.findAll({
        where: { product_status: 1},
        order: [
            ['product_count', 'DESC']
        ],
        limit: 6
    })
    if(getproducts.length > 0){
        const products = []
        for (let index = 0; index < getproducts.length; index++) {
            const product = {
                product_id: getproducts[index]['product_id'],
                product_count: getproducts[index]['product_count'],
                product_serial: getproducts[index]['product_serial'],
                product_name: getproducts[index]['product_name'],
                product_price: getproducts[index]['product_price'],
                product_image: endPoint + imgProduct + getproducts[index]['product_image'],
                product_detail: getproducts[index]['product_detail'],
            }
            products.push(product)
        }
        return res.json(products)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetNewProduct = async function (req, res ,next) {
    const getproducts = await Product.findAll({
        where: { product_status: 1},
        order: [
            ['product_id', 'DESC']
        ],
        limit: 6
    })
    if(getproducts.length > 0){
        const products = []
        for (let index = 0; index < getproducts.length; index++) {
            const product = {
                product_id: getproducts[index]['product_id'],
                product_count: getproducts[index]['product_count'],
                product_serial: getproducts[index]['product_serial'],
                product_name: getproducts[index]['product_name'],
                product_price: getproducts[index]['product_price'],
                product_image: endPoint + imgProduct + getproducts[index]['product_image'],
                product_detail: getproducts[index]['product_detail'],
            }
            products.push(product)
        }
        return res.json(products)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetActviveProduct = async function (req, res ,next) {
    const getproducts = await Product.findAll({
        where: { product_status: 1},
    })
    if(getproducts && getproducts.length > 0){
        const products = []
        for (let index = 0; index < getproducts.length; index++) {
            const product = {
                product_id: getproducts[index]['product_id'],
                product_count: getproducts[index]['product_count'],
                product_serial: getproducts[index]['product_serial'],
                product_name: getproducts[index]['product_name'],
                product_price: getproducts[index]['product_price'],
                product_image: endPoint + imgProduct + getproducts[index]['product_image'],
                product_detail: getproducts[index]['product_detail'],
                category: getproducts[index]['category']
            }
            products.push(product)
        }

        return res.json(products)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetProductByCategory = async function (req, res ,next) {
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const getproducts = await Product.findAll({
        where: { category_id: req.params.id }
    })
    if( getproducts.length > 0){
        const products = []
        for (let index = 0; index < getproducts.length; index++) {
            const product = {
                product_id: getproducts[index]['product_id'],
                product_count: getproducts[index]['product_count'],
                product_serial: getproducts[index]['product_serial'],
                product_name: getproducts[index]['product_name'],
                product_price: getproducts[index]['product_price'],
                product_image: endPoint + imgProduct + getproducts[index]['product_image'],
                product_detail: getproducts[index]['product_detail'],
                category: getproducts[index]['category']
            }
            products.push(product)
        }
        return res.json(products)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetAllProduct = async function (req, res ,next) {
    const getproducts = await Product.findAll({
        include: Category
    })
    if(getproducts.length > 0){
        const products = []
        for (let index = 0; index < getproducts.length; index++) {
            const product = {
                product_id: getproducts[index]['product_id'],
                product_count: getproducts[index]['product_count'],
                product_serial: getproducts[index]['product_serial'],
                product_cost: getproducts[index]['product_cost'],
                product_qty: getproducts[index]['product_qty'],
                product_status: getproducts[index]['product_status'],
                product_create_at: getproducts[index]['product_create_at'],
                product_name: getproducts[index]['product_name'],
                product_price: getproducts[index]['product_price'],
                product_image: endPoint + imgProduct + getproducts[index]['product_image'],
                product_detail: getproducts[index]['product_detail'],
                category: getproducts[index]['category']
            }
            products.push(product)
        }

        return res.json(products)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetProduct = async function (req, res ,next) {
    if(!req.params.id){
        return res.status(400).send({
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const findproduct = await Product.findOne({
        where: { product_id: req.params.id }
    })
    if (findproduct){
        return res.json(findproduct)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const AddProduct = async function (req, res ,next) {
    if(!req.body.name, !req.body.price, !req.body.cost, !req.body.serial, !req.files){
        return res.status(400).send({
            status: 400,
            message: "ข้อมูลไม่ครบถ้วนกรุณากรอกใหม่!"
        })
    }
    var file = req.files.image
    var fileanmemd5 = file.md5
    var type = file.mimetype
    var cuttype = type.split('/')

    const findproduct = await Product.findOne({
        where: { product_name: req.body.name}
    })
    if (!findproduct){
        file.mv('./public/img/product/'+ fileanmemd5 + "." + cuttype[1], async function(err){
            if(err){
                console.log(err);
                return res.status(400).send({
                    status: 400,
                    message: "อัพโหลดรูปผิดพลาด"
                })
            }
            // {
            //     "serial": "A0000023323",
            //     "name": "เสื้อพรม+2",
            //     "cost": 10,
            //     "price": 20,
            //     "qty": 10,
            //     "detail": "",
            //     "category": 1
            // }
            const result = await Product.create({
                category_id: req.body.category,
                product_serial: req.body.serial,
                product_name: req.body.name,
                product_cost: req.body.cost,
                product_price: req.body.price,
                product_qty: req.body.qty, 
                product_image: req.body.image,
                product_status: req.body.status,
                product_dtail: req.body.detail,
                product_image: fileanmemd5 + "." + cuttype[1]
            })
            if(result){
                return res.send({
                    message: "เพิ่มข้อมูลสำเร็จ"
                })
            }else{
                return res.status(400).send({
                    status: 400,
                    message: "ผิดพลาดไม่สามารถเพิ่มข้อมูลได้"
                })
            }
        })
    }else{
        return res.status(400).send({
            status: 400,
            message: "ชื่อสินค้าซ้ำ"
        })
    }
}

const UpdateProduct = async function (req, res ,next) {
    if(!req.params.id){
        return res.status(400).send({
            status: 400,
            message: "ไม่พบข้อมูล"
        })
    }
    const find = await Product.findOne({
        where: { product_id: req.params.id}
    })
    if(find){
        if(req.files){
            var file = req.files.image
            var fileanmemd5 = file.md5
            var type = file.mimetype
            var cuttype = type.split('/')

            file.mv('./public/img/product/'+ fileanmemd5 + "." + cuttype[1], async function(err){
                if(err){
                    console.log(err);
                    return res.status(400).send({
                        status: 400,
                        message: "อัพโหลดรูปผิดพลาด"
                    })
                }
                const result = await Product.update({
                    category_id: req.body.category,
                    product_serial: req.body.serial,
                    product_name: req.body.name,
                    product_cost: req.body.cost,
                    product_price: req.body.price,
                    product_qty: req.body.qty, 
                    product_image: req.body.image,
                    product_status: req.body.status,
                    product_image: fileanmemd5 + "." + cuttype[1]
                })
                if(result){
                    return res.send({
                        message: "แก้ไขข้อมูลสำเร็จ"
                    })
                }else{
                    return res.status(400).send({
                        status: 400,
                        message: "ผิดพลาดไม่สามารถแก้ไขข้อมูลได้"
                    })
                }
            })
        }else{
            const data = await Product.update({
                category_id: req.body.category,
                product_serial: req.body.serial,
                product_name: req.body.name,
                product_cost: req.body.cost,
                product_price: req.body.price,
                product_qty: req.body.qty, 
                product_image: req.body.image,
                product_status: req.body.status,
                product_dtail: req.body.detail
            },{
                where: { product_id: req.params.id }
            })
            if(data){
                return res.send({
                    message: "แก้ไขสำเร็จ"
                })
            }else{
                return res.status(400).send({
                    status: 400,
                    message: "แก้ไขผิดพลาด"
                })
            }
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบข้อมูล"
        })
    }
}

const RemoveProduct = async function (req, res ,next) {
    if(!req.params.id){
        return res.status(400).send({
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const findproduct = await Product.findOne({
        where: { product_id: req.params.id, [Op.or]:[{product_status: 1}, {product_status: 2}] }
    })
    if (findproduct){
        const removeproduct = await Product.update({ product_status: 3} , {
            where: { product_id: req.params.id}
        })
        if(removeproduct){
            return res.send({
                message: "ลบข้อมูลสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "ลบข้อมูลผิดพลาด"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

module.exports = {
    GetBestSellerProduct,
    GetNewProduct,
    GetActviveProduct,
    GetAllProduct,
    GetProduct,
    AddProduct,
    RemoveProduct,
    UpdateProduct,
    GetProductByCategory
}