const db = require('../../db/index')
const { Cart, Cart_detail, Users, Product } = db
const { Op, where } = require('sequelize')

const GetItemCart = async function (req, res, next) {
    const user = await Users.findOne({
        where: { user_uuid: req.userData.uuid }
    })
    if(user){
        const findcart = await Cart.findOne({
            where: { [Op.and]:[{ cart_status: 1 , user_id: user.user_id}]},
        })
        // return res.json(findcart)
        if(findcart){
            const calamt = await Cart_detail.sum('cart_detail_amt',{
                where: {cart_id: findcart.cart_id}
            })
            
            const updatecartamt = await Cart.update({
                cart_amt: calamt
            }, {where: { cart_id: findcart.cart_id }})

            const cartdata = await Cart.findOne({
                where: { cart_id: findcart.cart_id },
                include: [{
                    model: Cart_detail,
                    include: Product
                }]
            })
            
            return res.json(cartdata)
        }else{
            return res.status(400).send({
                status: 400,
                message: "ไม่พบตะกร้าสินค้า"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบผู้ใช้งาน"
        })
    }
}

const AddItemCart = async function (req, res, next) {
    if(!req.body.item, !req.body.count){
        return res.status(400).send({
            status: 400,
            message: "ไม่มีสินค้าที่เพิ่ม"
        })
    }
    const user = await Users.findOne({
        where: { user_uuid: req.userData.uuid }
    })
    if(user){
        const findcart = await Cart.findOne({
            where: { [Op.and]:[{ cart_status: 1 , user_id: user.user_id}] }
        })
        if(findcart){
            const finditem = await Product.findOne({
                where: { product_id: req.body.item }
            })
            if (finditem){
                if(finditem.product_qty >= req.body.count){
                    const finddetail = await Cart_detail.findOne({
                        where: { [Op.and]: [ {cart_id: findcart.cart_id}, {product_id :finditem.product_id} ] }
                    })
                    if(finddetail){
                        const updatedetail = await Cart_detail.update({
                            cart_detail_qty: finddetail.cart_detail_qty + req.body.count,
                            cart_detail_amt: finditem.product_price * req.body.count + parseInt(finddetail.cart_detail_amt)},{
                                where: { [Op.and]: [ {product_id: finditem.product_id}, {cart_id: findcart.cart_id} ]}
                        })
                        if(updatedetail){                            
                            return res.send({
                                message: "เพิ่มลงตะกร้าสินค้าสำเร็จ"
                            })
                        }else{
                            return res.status(400).send({
                                status: 400,
                                message: "เพิ่มลงตะกร้าสินค้าไม่สำเร็จ"
                            })
                        }
                    }else{
                        const adddetail = await Cart_detail.create({
                            cart_detail_qty: req.body.count,
                            cart_detail_amt: req.body.count * finditem.product_price,
                            cart_id: findcart.cart_id,
                            product_price: finditem.product_price,
                            product_id: finditem.product_id
                        })
                        if(adddetail){
                            return res.send({
                                message: "เพิ่มลงตะกร้าสำเร็จ"
                            })
                        }else{
                            return res.status(400).send({
                                stauts: 400,
                                message: "ระบบผิดพลาด"
                            })
                        }
                    }
                }else{
                    return res.status(400).send({
                        status: 400,
                        message: "สินค้าไม่เพียงพอ"
                    })
                }
            }else{
                return res.status(400).send({
                    status: 400,
                    message: "ไม่พบตะกร้าสินค้า"
                })
            }
        }else{
            const finditem = await Product.findOne({
                where: { product_id: req.body.item }
            })
            if (finditem){
                if(finditem.product_qty >= req.body.count){
                    const addrow = await Cart.create({
                        cart_status: 1,
                        user_id: user.user_id
                    })
                    if(addrow){
                        const adddetail = await Cart_detail.create({
                            cart_detail_qty: req.body.count,
                            cart_detail_amt: req.body.count * finditem.product_price,
                            cart_id: addrow.cart_id,
                            product_price: finditem.product_price,
                            product_id: finditem.product_id
                        })
                        if(adddetail){
                            return res.send({
                                message: "เพิ่มลงตะกร้าสำเร็จ"
                            })
                        }else{
                            return res.status(400).send({
                                stauts: 400,
                                message: "ระบบผิดพลาด"
                            })
                        }
                    }else{
                      return res.status(400).send({
                            status: 400,
                            message: "ระบบผิดพลาด"
                        })  
                    }
                }else{
                    return res.status(400).send({
                        status: 400,
                        message: "สินค้าไม่เพียงพอ"
                    })
                }
            }else{
                return res.status(400).send({
                    status: 400,
                    message: "ไม่พบสินค้า"
                })
            }
            
        }
    }
    return res.status(400).send({
        status: 400,
        message: "ไม่พบผู้ใช้งาน"
    })
}

const RemoveItemCart = async function (req, res, next) {
    if(!req.body.item){
        return res.status(400).send({
            status: 400,
            message: "ไม่พบสินค้าที่ต้องการนำออกจากตะกร้าสินค้า"
        })
    }
    const finddetail = await Cart_detail.findOne({
        where: { cart_detail_id: req.body.item }
    })
    if(finddetail){
        const deldetal = await Cart_detail.destroy({
            where: { cart_detail_id:finddetail.cart_detail_id }
        })
        if(deldetal){
            return res.send({
                message: "ลบสินค้าออกจากตะกร้าสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "ลบสินค้าออกจากตะกร้าไม่สำเร็จ"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่พบสินค้าในตะกร้าสินค้า"
        })
    }
}

module.exports = {
    GetItemCart,
    AddItemCart,
    RemoveItemCart
}