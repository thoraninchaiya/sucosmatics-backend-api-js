const db = require('../../db/index')
const { Carousel } = db
const { Op } = require('sequelize')
const { imgCarousel, endPoint } = require('../../config')

const GetActviveCarousel = async function (req, res, next) {
    const getcarousels = await Carousel.findAll({
        where: { carousel_status: 1}
    })
    if(getcarousels.length > 0){
        const carousels = []
        for (let index = 0; index < getcarousels.length; index++) {
            const carousel = {
                carousel_id: getcarousels[index]['carousel_id'],
                carousel_image: endPoint + imgCarousel + getcarousels[index]['carousel_image'],
                carousel_status: getcarousels[index]['carousel_status'],
            }
            carousels.push(carousel)
        }
        return res.json(carousels)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const GetAllCarousel = async function (req, res, next) {
    const getcarousels = await Carousel.findAll()
    if(getcarousels.length > 0){
        const carousels = []
        for (let index = 0; index < getcarousels.length; index++) {
            const carousel = {
                carousel_id: getcarousels[index]['carousel_id'],
                carousel_image: endPoint + imgCarousel + getcarousels[index]['carousel_image'],
                carousel_status: getcarousels[index]['carousel_status'],
            }
            carousels.push(carousel)
        }
        return res.json(carousels)
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

const AddCarousel = async function (req, res, next) {
    if(!req.files){
        return res.status(400).send({
            status: 400,
            message: "ข้อมูลไม่ครบถ้วนกรุณากรอกใหม่!"
        })
    }
    var file = req.files.image
    var fileanmemd5 = file.md5
    var type = file.mimetype
    var cuttype = type.split('/')
    file.mv('./public/img/carousel/'+ fileanmemd5 + "." + cuttype[1], async function(err){
        if(err){
            console.log(err);
            return res.status(400).send({
                status: 400,
                message: "อัพโหลดรูปผิดพลาด"
            })
        }else{
            const result = await Carousel.create({
                carousel_status: req.body.status,
                carousel_image: fileanmemd5 + "." + cuttype[1]
            })
            if(result){
                return res.send({
                    message: "เพิ่มข้อมูลสำเร็จ"
                })
            }
        }
    })
}

const RemoveCarousel = async function (req, res, next) {
    if(!req.params.id){
        return res.status(400).send({
            message: "กรุณากรอกข้อมูให้ครบถ้วน"
        })
    }
    const findcarousel = await Carousel.findOne({
        where: { carousel_id: req.params.id}
    })
    if (findcarousel){
        const removecarousel = await Carousel.destroy({
            where: {carousel_id: req.params.id}
        })
        if(removecarousel){
            return res.send({
                message: "ลบข้อมูลสำเร็จ"
            })
        }else{
            return res.status(400).send({
                status: 400,
                message: "ลบข้อมูลผิดพลาด"
            })
        }
    }else{
        return res.status(400).send({
            status: 400,
            message: "ไม่มีข้อมูล"
        })
    }
}

module.exports = {
    GetActviveCarousel,
    GetAllCarousel,
    AddCarousel,
    RemoveCarousel,
}