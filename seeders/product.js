"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "product",
      [
        {
          category_id: 1,
          product_serial: "S01",
          product_name: "Lip Lover",
          product_cost: 40,
          product_price: 59,
          product_qty: 124,
          product_status: 1,
          product_image: "Product01.png",
          product_detail: "ทาได้ทั้ง ตา แก้ม ปาก เนื้อเกลี่ยง่าย  ติดทน กันน้ำสุดๆ คนปากแห้งก็ทาได้",
        },
        {
          category_id: 1,
          product_serial: "S02",
          product_name: "v Acne Clear Booter",
          product_cost: 50,
          product_price: 500,
          product_qty: 10,
          product_status: 1,
          product_image: "Product01.png",
          product_detail: "",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("product", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
