'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('users', [
      {
        user_uuid: '637511f5-25a9-4cf4-af86-a2be7363780b',
        user_email: 'thoranin@gmail.com',
        user_password: '$2a$10$Xsaez2jUR4Qn9QBmR619iuXUgYnfWGB2.sAu8j49sGld4Dtz7sLey',
        user_fname: 'thoranin',
        user_lname: 'thoranin',
        user_phone: '1234567890',
        user_address: '1234',
        user_status: 'active',
        user_role: 'member',
      },
      {
        user_uuid: '64d4274a-1465-473a-9817-c7d57b9b1748',
        user_email: 'saichon@gmail.com',
        user_password: '$2a$10$Xsaez2jUR4Qn9QBmR619iuXUgYnfWGB2.sAu8j49sGld4Dtz7sLey',
        user_fname: 'saichon',
        user_lname: 'saichon',
        user_phone: '1234567890',
        user_address: '1234',
        user_status: 'active',
        user_role: 'member',
      }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('users', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
