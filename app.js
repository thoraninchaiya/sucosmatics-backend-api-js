const express = require("express")
const app = express();
app.use(express.json());
const cors = require('cors')
app.use(cors())
var path = require('path');
let port = 4000
const db = require('./db/index.js');
const { player, team } = db
const mysql = require('mysql2/promise');
require('dotenv').config()
var fileupload = require('express-fileupload')
app.use(fileupload())

app.use('/image', express.static(path.join(__dirname, 'public/img')))

// db.sequelize.sync({force: false});
initialize();

async function initialize() {
    const connection = await mysql.createConnection({
        host: process.env.db_connection,
        port: process.env.db_port,
        user: process.env.db_username,
        password: process.env.db_password
    });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${process.env.db_database_name}\`;`);
    await db.sequelize.sync({ force: false });
}

const router = require('./routers/index')
app.use('/api', router)

app.get('/playerInfo', async (req, res) => {
    info = await player.findAll({
        include: team
    });
    // console.log(info);
    res.json(info);
});

app.get('/playerInfo/:id', async (req, res) => {
    id = req.params.id;
    info = await player.findOne({
        //attributes: ['name', ['tid','team'] , 'age'], สามารถเลือกเฉพาะ attributes ที่ต้องการได้ และ ['tid','team'] เขียนเป็น sql ก็จะได้ แบบนี้ครับ tid AS team
        where: { pid: id }
    });
    if (!info) {
        res.sendStatus(500);
    } else {
        res.json(info);
    }
});

app.post('/playerInfo', async (req, res) => {
    data = req.body.data;
    info = await player.create({
        name: data.name,
        age: data.age,
        position: data.position,
        tid: data.tid,
    });
    if (!info) {
        res.sendStatus(500);
    } else {
        res.status(200).json(info);
    }
});

app.put('/playerInfo/:id', async (req, res) => {
    id = req.params.id;
    info = await player.update({ position: 'ST' }, {
        where: { pid: id }
    });
    if (!info) {
        res.sendStatus(500);
    } else {
        res.sendStatus(200);
    }
});

app.delete('/playerInfo/:id', async (req, res) => {
    id = req.params.id;
    info = await player.destroy({
        where: { pid: id }
    });
    if (!info) {
        res.sendStatus(500);
    } else {
        res.sendStatus(200);
    }
});

app.post('/test/upload', (req, res) => {
    console.log(req.files);
    console.log(req.body)
    if (req.files) {
        var file = req.files.test
        if (!file) {
            return res.status(400).send({
                message: "ไม่พบรูป"
            })
        }
        var fileanmemd5 = file.md5
        var type = file.mimetype
        var cuttype = type.split('/')

        file.mv('./public/img/' + fileanmemd5 + "." + cuttype[1], function(err) {
            if (err) {
                res.send(err)
            } else {
                res.send("file upload")
            }
        })
    }
})


// app.use((_req, res) => {
//     res.sendStatus(501);
// });

app.get('*', function(req, res){
    res.send('Page Not Found!', 404);
});

app.listen(port, () => {
    console.log("sever run on port " + port);
});