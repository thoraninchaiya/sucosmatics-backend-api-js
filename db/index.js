const { Sequelize } = require('sequelize')

//อันนี้เป็นส่วนที่ใช้ในการบอก Sequelize ว่าเราจะ connect ไปที่ไหน
const sequelize = new Sequelize(
    process.env.db_database_name, // นี่เป็นชื่อ DB ของเรานะครับ
    process.env.db_username, // user ที่ใช้สรการเข้าไปยัง db
    process.env.db_password, // password
    {
        host: process.env.db_connection, // host ของ db ที่เราสร้างเอาไว้
        dialect: 'mysql', // 'mysql' | 'mariadb' | 'postgres' | 'mssql'   พวกนี้ใช่ก็ใช้ได้นะจ๊ะ
        define: {
            timestamps: false //ส่วนตรงนี้ก็เป็นการตั้งค่าเพิ่มเติม
        },
        logging: true,
    })

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

//ส่วนนี้เป็นการ import model ของ table ใน database เข้ามาเพื่อตั้งต่า relation นะครับ
db.Users = require("./model/Users")(sequelize, Sequelize)
db.Category = require("./model/product_category")(sequelize, Sequelize)
db.Delivery_company = require("./model/delivery_category_company")(sequelize, Sequelize)
db.Delivery = require("./model/delivery")(sequelize, Sequelize)
db.Product = require("./model/product")(sequelize, Sequelize)
db.Receipt = require("./model/receipt")(sequelize, Sequelize)
db.Receipt_detail = require("./model/receipt_detail")(sequelize, Sequelize)
db.Order = require("./model/order")(sequelize, Sequelize)
db.Order_detail = require("./model/order_detail")(sequelize, Sequelize)
db.Review = require("./model/review")(sequelize, Sequelize)
db.Review_image = require("./model/review_image")(sequelize, Sequelize)
db.Payment = require("./model/payment")(sequelize, Sequelize)
db.Cart = require("./model/cart")(sequelize, Sequelize)
db.Cart_detail = require("./model/cart_detail")(sequelize, Sequelize)
db.team = require("./model/team")(sequelize, Sequelize)
db.player = require("./model/player")(sequelize, Sequelize)
db.Carousel = require("./model/carousel")(sequelize, Sequelize)

//ส่วนนี้เป็นการตั้งต่า relation นะครับ โดยเป็นการบอกว่าใน 1 team มีได้หลาย player ง่ายๆ ก็คือ relation แบบ 1:M 
db.team.hasMany(
    db.player,
    {
        foreignKey: { name: 'tid', field: 'tid' }, //name ตรงสำคัญพยายามตั่งให้เป็นชื่อเดียวกับ FK ใน table ที่นำไปใช้นะครับ
    }
)
db.Delivery_company.hasMany(
    db.Delivery,{
        foreignKey: { name: 'delivery_company_id', field: 'delivery_company_id' }
    },
)
db.Delivery.hasMany(
    db.Order,{
        foreignKey: { name: 'delivery_id', field: 'delivery_id' }
    }
)
db.Category.hasMany(
    db.Product,{
        foreignKey: { name: 'category_id', field: 'category_id' }
    }
)

db.Receipt.hasMany(
    db.Receipt_detail,{
        foreignKey: { name: 'receipt_id', field: 'receipt_id' }
    }
)

db.Product.hasMany(
    db.Order_detail,{
        foreignKey: { name: 'product_id', field: 'product_id' }
    },
    db.Review,{
        foreignKey: { name: 'product_id', field: 'product_id'}
    },
    db.Cart_detail,{
        foreignKey: { name: 'product_id', field: 'product_id'}
    } 
)
db.Review.hasMany(
    db.Review_image,{
        foreignKey: { name: 'review_image_id', field: 'review_image_id' }
    }
)

db.Order.hasMany(
    db.Order_detail,{
        foreignKey: { name: 'order_id', field: 'order_id' }
    },
    db.Payment,{
        foreignKey: { name: 'order_id', field: 'order_id'}
    }
)

db.Users.hasMany(
    db.Review,{
        foreignKey: { name: 'user_id', field: 'user_id'}
    },
    db.Order, {
        foreignKey: { name: 'user_id', field: 'user_id'}
    },
    db.Cart, {
        foreignKey: { name: 'user_id', field: 'user_id'}
    }
)

db.Cart.hasMany(
    db.Cart_detail,{
        foreignKey: { name: 'cart_id', field: 'cart_id'}
    }
)

//ส่วนนี้เป็นการตั้ง relation แบบกลับกันกับด้านบน จริงแล้วเราไม่ตั้งก็ได้นะครับแต่ผมแนะนำให้ตั้งเอาไว้ เพราะเวลาที่เราไม่ได้ใส่ 
//line นี้จะทำให้เราสามารถใช้  team ในการหา player ได้อย่างเดียวและไม่สามารถใช้ player หา team ได้
db.player.belongsTo(db.team, { foreignKey: 'tid' })
db.Delivery.belongsTo(db.Delivery_company, { foreignKey: 'delivery_company_id'})
db.Order.belongsTo(db.Delivery, { foreignKey: 'delivery_id'})
db.Product.belongsTo(db.Category, { foreignKey: 'category_id'})
db.Order_detail.belongsTo(db.Order, { foreignKey: 'order_id'})
db.Order_detail.belongsTo(db.Product, { foreignKey: 'product_id'})
db.Review_image.belongsTo(db.Review, { foreignKey: 'review_id'})
db.Review.belongsTo(db.Product, { foreignKey: 'product_id'})
db.Order.belongsTo(db.Users, { foreignKey: 'user_id'})
db.Cart.belongsTo(db.Users, { foreignKey: 'user_id'})
db.Cart_detail.belongsTo(db.Product, { foreignKey: 'product_id'})
db.Cart_detail.belongsTo(db.Cart, { foreignKey: 'cart_id'})
db.Receipt_detail.belongsTo(db.Receipt, { foreignKey: 'receipt_id'})
db.Payment.belongsTo(db.Order, { foreignKey: 'order_id'})


module.exports = db