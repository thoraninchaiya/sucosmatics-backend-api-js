module.exports = (sequelize, Sequelize) => {
    const review_image = sequelize.define(
        'review_image',
        {
            review_image_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'review_image_id' },
            review_image_img: { type: Sequelize.STRING(50), field: 'review_image_img' },
        },
        {
            tableName: 'review_image'
        }
    )

    return review_image
}