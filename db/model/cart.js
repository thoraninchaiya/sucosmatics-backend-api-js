module.exports = (sequelize, Sequelize) => {
    const cart = sequelize.define(
        'cart',
        {
            cart_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'cart_id' },            
            cart_date: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), allowNull: false, field: 'cart_date' },
            cart_amt: { type: Sequelize.DECIMAL, allowNull: false, field: 'cart_amt', defaultValue: 0},
            cart_status: { type: Sequelize.ENUM, values: ['pending', 'success', 'cancel'], defaultValue: 'pending', allowNull: false, field: 'cart_status' },
        },
        {
            tableName: 'cart'
        }
    )

    return cart
}