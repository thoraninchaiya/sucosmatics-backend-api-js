module.exports = (sequelize, Sequelize) => {
    const category = sequelize.define(
        'category',
        {
            category_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'category_id' },
            category_name_th: { type: Sequelize.STRING(50), allowNull: true, field: 'category_name' },
            category_status: { type: Sequelize.ENUM, values: ['active', 'unactive', 'delete'], defaultValue: 'active', allowNull: false, field: 'category_status' },
            category_name_en: { type: Sequelize.STRING(50), allowNull: true, field: 'category_name_en' },
            category_create_at: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), allowNull: false, field: 'category_create_at' },
        },
        {
            tableName: 'category'
        }
    )

    return category
}