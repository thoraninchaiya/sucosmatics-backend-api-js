module.exports = (sequelize, Sequelize) => {
    const receipt_detail = sequelize.define(
        'receipt_detail',
        {
            receipt_detail_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'receipt_detail_id' },
            receipt_qty: { type: Sequelize.INTEGER(11), allowNull: false, field: 'receipt_qty' },
            product_name: { type: Sequelize.STRING(50), allowNull: false, field: 'product_name' },
            product_unit_price: { type: Sequelize.DECIMAL, allowNull: false, field: 'product_unit_price' },
            product_total_amt: { type: Sequelize.DECIMAL, allowNull: false, field: 'product_total_amt' },
            product_serial: { type: Sequelize.STRING(50), allowNull: false, field: 'product_serial' },
        },
        {
            tableName: 'receipt_detail'
        }
    )

    return receipt_detail
}