module.exports = (sequelize, Sequelize) => {
    const payment = sequelize.define(
        'payment',
        {
            payment_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'payment_id' },
            payment_date: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), allowNull: false, field: 'payment_date' },
            payment_total: { type: Sequelize.DECIMAL, allowNull: false, field: 'payment_total' },            
            payment_image: { type: Sequelize.Sequelize.STRING(50), allowNull: false, field: 'payment_image' },            
            payment_time: { type: Sequelize.Sequelize.STRING(50), allowNull: false, field: 'payment_time' },            
        },
        {
            tableName: 'payment'
        }
    )

    return payment
}