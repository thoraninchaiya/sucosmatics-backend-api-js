module.exports = (sequelize, Sequelize) => {
    const cart_detail = sequelize.define(
        'cart_detail',
        {
            cart_detail_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'cart_detail_id' },
            cart_detail_amt: { type: Sequelize.DECIMAL, allowNull: false, field: 'cart_detail_amt' },
            cart_detail_qty: { type: Sequelize.INTEGER(11), field: 'cart_detail_qty' },
            product_price: { type: Sequelize.DECIMAL, allowNull: false, field: 'product_price' },
        },
        {
            tableName: 'cart_detail'
        }
    )

    return cart_detail
}