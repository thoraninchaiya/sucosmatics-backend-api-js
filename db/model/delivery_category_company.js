module.exports = (sequelize, Sequelize) => {
    const Delivery_Company = sequelize.define(
        'delivery_company',
        {
            delivery_company_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'delivery_company_id' },
            delivery_company_name: { type: Sequelize.STRING(50), allowNull: false, field: 'delivery_company_name' },
            delivery_company_status: { type: Sequelize.ENUM, values: ['active', 'unactive'], defaultValue: 'active', allowNull: false, field: 'delivery_company_status' },
        },
        {
            tableName: 'delivery_company'
        }
    )

    return Delivery_Company
}