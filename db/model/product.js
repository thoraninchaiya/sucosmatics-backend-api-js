module.exports = (sequelize, Sequelize) => {
    const product = sequelize.define(
        'product',
        {
            product_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'product_id' },
            product_count: { type: Sequelize.INTEGER(11), allowNull: false, defaultValue: 0, field: 'product_count' },
            product_serial: { type: Sequelize.STRING(50), allowNull: false, field: 'product_serial' },
            product_name: { type: Sequelize.STRING(50), allowNull: false, field: 'product_name' },
            product_cost: { type: Sequelize.DECIMAL, allowNull: true, field: 'product_cost' },
            product_price: { type: Sequelize.DECIMAL, allowNull: false, field: 'product_price' },
            product_qty: { type: Sequelize.STRING(50), allowNull: false, field: 'product_qty' },
            product_status: { type: Sequelize.ENUM, values: ['active', 'unactive', 'delete'], defaultValue: 'active', allowNull: false, field: 'product_status' },
            product_image: { type: Sequelize.STRING(50), allowNull: true, field: 'product_image', defaultValue: 'default_item.jpg' },
            product_detail: { type: Sequelize.STRING(50), allowNull: true, field: 'product_detail' },
            product_create_at: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), allowNull: false, field: 'product_create_at' },
        },
        {
            tableName: 'product'
        }
    )

    return product
}