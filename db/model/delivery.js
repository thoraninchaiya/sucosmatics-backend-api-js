module.exports = (sequelize, Sequelize) => {
    const delivery = sequelize.define(
        'delivery',
        {
            delivery_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'delivery_id' },
            delivery_serial: { type: Sequelize.STRING(50), allowNull: true, field: 'delivery_serial' },
            delivery_date: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), allowNull: true, field: 'delivery_date' },
        },
        {
            tableName: 'delivery'
        }
    )

    return delivery
}