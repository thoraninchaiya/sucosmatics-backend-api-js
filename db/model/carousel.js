module.exports = (sequelize, Sequelize) => {
    const carousel = sequelize.define(
        'carousel',
        {
            carousel_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'carousel_id' },
            carousel_image: { type: Sequelize.STRING(50), allowNull: true, field: 'carousel_image' },
            carousel_status: { type: Sequelize.ENUM, allowNull: false, values: ['active', 'unactive'], defaultValue: 'active', field: 'carousel_status' },
        },
        {
            tableName: 'carousel'
        }
    )

    return carousel
}