module.exports = (sequelize, Sequelize) => {
    const users = sequelize.define(
        'users',
        {
            user_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'user_id' },
            user_uuid: { type: Sequelize.STRING(50), allowNull: true, field: 'user_uuid' },
            user_email: { type: Sequelize.STRING(50), allowNull: true, field: 'user_email' },
            user_password: { type: Sequelize.STRING(100), allowNull: true, field: 'user_password' },
            user_fname: { type: Sequelize.STRING(50), allowNull: true, field: 'user_fname' },
            user_lname: { type: Sequelize.STRING(50), allowNull: true, field: 'user_lname' },
            user_phone: { type: Sequelize.STRING(15), allowNull: true, field: 'user_phone' },
            user_address: { type: Sequelize.STRING(100), allowNull: true, field: 'user_address' },
            user_status: { type: Sequelize.ENUM, values: ['active', 'unactive', 'banned'], defaultValue: 'active', allowNull: false, field: 'user_status' },
            user_create_at: { type: Sequelize.DATE, allowNull: false, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), field: 'user_create_at' },
            user_role: { type: Sequelize.ENUM, allowNull: false, values: ['member', 'admin'], defaultValue: 'member', field: 'user_role' },
        },
        {
            tableName: 'users'
        }
    )

    return users
}