module.exports = (sequelize, Sequelize) => {
    const order = sequelize.define(
        'order',
        {
            order_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'order_id' },
            order_serial: { type: Sequelize.STRING(50), allowNull: false, field: 'order_serial' },
            order_date: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), allowNull: false, field: 'order_date' },
            order_total_amt: { type: Sequelize.DECIMAL, allowNull: false, field: 'order_total_amt' },
            order_status: { type: Sequelize.ENUM, values: ['waitpaid', 'pending', 'success', 'cancel', 'delivery'], defaultValue: 'waitpaid', allowNull: false, field: 'order_status' },
        },
        {
            tableName: 'order'
        }
    )

    return order
}
//cart_details