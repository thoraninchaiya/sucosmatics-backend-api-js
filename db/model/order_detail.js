module.exports = (sequelize, Sequelize) => {
    const order_detail = sequelize.define(
        'order_detail',
        {
            order_detail_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'order_detail_id' },
            order_qty: { type: Sequelize.INTEGER(11), allowNull: false, field: 'order_qty' },
            product_unit_price: { type: Sequelize.DECIMAL, allowNull: false, field: 'product_unit_price' },
            product_total_amt: { type: Sequelize.DECIMAL, allowNull: false, field: 'product_total_amt' },
        },
        {
            tableName: 'order_detail'
        }
    )

    return order_detail
}