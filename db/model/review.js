module.exports = (sequelize, Sequelize) => {
    const review = sequelize.define(
        'review',
        {
            review_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'review_id' },
            review_detail: { type: Sequelize.TEXT, allowNull: true, field: 'review_detail' },
            review_rating: { type: Sequelize.INTEGER(11), field: 'review_rating' },
            review_date: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), field: 'review_date' },
            review_status: { type: Sequelize.INTEGER(11), field: 'review_status' },
        },
        {
            tableName: 'review'
        }
    )

    return review
}