module.exports = (sequelize, Sequelize) => {
    const receipt = sequelize.define(
        'receipt',
        {
            receipt_id: { type: Sequelize.INTEGER(11), primaryKey: true, autoIncrement: true, field: 'receipt_id' },
            receipt_serial: { type: Sequelize.STRING(50), allowNull: false, field: 'receipt_serial' },
            receipt_date: { type: Sequelize.DATE, defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'), allowNull: false, field: 'receipt_date' },
            receipt_total_amt: { type: Sequelize.DECIMAL, allowNull: false, field: 'receipt_total_amt' },
            user_address: { type: Sequelize.STRING(50), autoIncrement: false, field: 'user_address' },
            user_name: { type: Sequelize.STRING(50), autoIncrement: false, field: 'user_name' },
        },
        {
            tableName: 'receipt'
        }
    )

    return receipt
}