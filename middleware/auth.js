const { jwtverify } = require("../utils/jwt")

const isLoggedIn = async function(req, res, next) {
    console.log(req.headers);
    if (!req.headers.authorization) {
        return res.status(400).send({
            message: "กรุณาเข้าสู่ระบบ",
            status: 400
        })
    } else {
        try {
            const authHeader = req.headers.authorization
            const token = authHeader.split(' ')[1]
            const decoded = await jwtverify(token)
            req.userData = decoded
            next()
        } catch (err) {
            return res.status(401).send({
                message: "กรุณาเข้าสู่ระบบ",
                status: 400
            })
        }
    }
}

module.exports = {
    isLoggedIn
}