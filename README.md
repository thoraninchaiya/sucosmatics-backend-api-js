
## First Setup
for first install 
```
npm i
```

## Start Project
```
docker-compose up -d
``` 

## Open logs
open api logs
```
docker-compose logs -f api
```
open mysql logs
```
docker-compose logs -f db
```

## Seeds
make database seeds:
```
npx sequelize seed:generate --name "<seed name>"
```
push database seeds:
```
npx sequelize db:seed --seed "<seed name>"
```

## How to git
```
add file
$ git add .

check edit list file
$ git status

commit file
$ git commit -m "<reason or comment or update list>"

push file
$ git push
```