FROM node:14-alpine
WORKDIR /home/sucosmatic
COPY package.json package-lock.json
ADD . . 
RUN npm install
CMD ["npm","run","dev"]